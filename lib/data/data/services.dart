import 'package:anerny/data/data_struct.dart';


class ElectronicServices {


  static var dawrat = Article(
    title: "خدمة طلب دورة تدريبية",
    content: """توفر مكتبة جامعة طيبة لجميع المستفيدين من أعضاء هيئة التدريس و الطلاب والطالبات خدمة طلب دورة تدريبية إما لشرح كيفية التسجيل في المكتبة الرقمية أو دورة لشرح قواعد بيانات معينة وغيرها من الدورات, علما بان اقل عدد للمتقدمين للدورة ٨ اشخاص لجميع التخصصات. """,
    imageAssetPaths: [
      "assets/articles/services/dwrat/Picture1.png",
      "assets/articles/services/dwrat/Picture2.png",
    ]
  );

  static var hmm = Article(
    title: "خدمات ذوي الهمم",
    content: """تولي مكتبة جامعة طيبة عناية للمستخدمين من ذوي الهمم ,حيث توفر لهم الوسائل التي تساعدهم على الاستفادة من مصادر المكتبة المختلفة مثل الأجهزة السمعية والبصرية وغيرها ، كما خصصت متخصصين يقومون  بتلبية الطلب في أسرع وقت ممكن.""",
    imageAssetPaths: [
      "assets/articles/services/hmm/Picture1.png",
      "assets/articles/services/hmm/Picture2.png",
    ]
  );

  static var hgzM3ml = Article(
    title: "خدمة حجز معمل التدريب",
    content: """توفر هذه الخدمة إمكانية حجز قاعة تدريب بمكتبة الجامعة الرئيسية تسع إلى "١٥" طالبة، وبمكتبة الطلاب تسع إلى "٢٠" طالب ومكتبة السلام للطالبات تسع إلى " ٣٤" طالبة، وتتم تعبئة نموذج لهذه الخدمة بالعمادة بطلب من الكليات او الأقسام العلمية وتوجد هذه الخدمة في فرعي المركزية وكلية السلام ،  ويجب ان يكون عدد المتدربين لا يقل عن ١٠ ولا يزيد عن ٢٠. 
كيفية طلب الخدمة :""",
    imageAssetPaths: [
      "assets/articles/services/hgzM3ml/Picture1.png"
    ]
  );

  static var marg3ya = Article(
    title: "الخدمة الرقمية المرجعية",
    content: """الهدف من هذه الخدمة مساعدة  المستفيدين للحصول على المعلومات والرد على الاستفسارات  بأسرع وقت وأقل جهد، بالإضافة لوضع الحلول المناسبة للمشكلات التي تواجه المستفيدين ،   و تلبية احتياجاتهم المعلوماتية والحصول على المعلومات المطلوبة.
كيفية طلب الخدمة :""",
    imageAssetPaths: [
      "assets/articles/services/marg3ya/Picture1.png"
    ]
  );

  static var bahsBrith = Article(
    title: "خدمة البحث في  المكتبة البريطانية",
    content: """تنسق مكتبة جامعة طيبة طلب مواد علمية من المكتبة البريطانية وتوفرها مجانا بصيغة إلكترونية, والمكتبة الوطنية للمملكة المتحدة ومقرها لندن، هي إحدى أهم مراكز البحث المكتبية في العالم حيث أنها إحدى أكبر المكتبات في العالم من حيث حجم محتوياتها. تضم المكتبة نحو 150 مليون عنصر من كل بقاع العالم وبمختلف اللغات وبأشكال مختلفة، سواء المطبوعة أو الرقمية، مثل الكتب والمخطوطات والرسومات والمجلات والجرائد والتسجيلات الصوتية والتسجيلات الموسيقية والفيديوهات وبراءات الاختراع والخرائط والطوابع وقواعد البيانات وغيرها .
كيفية طلب الخدمة:""",
    imageAssetPaths: [
      "assets/articles/services/bhsBrith/Picture1.png"
    ]
  );

  static var tlbZyara = Article(
    title: "خدمة طلب زيارة للمكتبة",
    content: """تقدم هذه الخدمة للجميع ولا تشمل جهة معينة، بحيث يمكن لأعضاء هيئة التدريس والطلاب والطالبات تحديد موعد لزيارة المكتبة في كافة الفروع ، وتقوم عمادة شؤون المكتبات بعمل ترتيبات استقبال وشرح لكافة الإمكانيات الخدمات.
كيفية طلب الخدمة : """,
    imageAssetPaths: [
      "assets/articles/services/tlbZyara/Picture1.png"
    ]
  );

  static var other = Article(
    title: "خدمات آخرى",
    content: """ ==خدمة البحث في مصادر المعلومات
تقدم مكتبة جامعة طيبة خدمة البحث في مصادر المعلومات , هذه الخدمة تساهم في مساعدة الباحثين عن المصادر في موضوع بحث معين ما على المستفيد إلا أن يضع عنوان البحث وسوف يقوم المختص بإرسال جميع المصادر التي تفيد عنوان البحث  على بريد المستفيد , الفئة المستهدفة من الخدمة جميع مستفيدي جامعة طيبة .

==خدمه البحث عن الوثائق
تقدم هذه الخدمة البحث عن موضوع دراسة معينة لمؤلف معين وإرسالها إلكترونيا للمستفيد .

==خدمة مستودع جامعة طيبة الرقمي
يحتوي المستودع الرقمي على جميع إنتاج جامعة طيبة المعرفي من رسائل علمية ومؤتمرات وندوات وأبحاث لأعضاء هيئة التدريس وبإمكان جميع زوار البوابة الاستفادة من هذه الخدمات عن طريق زيارة الموقع.

==خدمة البحث عن رسالة علمية
 ضمن الخدمات التي تقدمها العمادة خدمة البحث عن رسالة علمية بجامعة طيبة
حيث أن جميع زوار بوابة عمادة شؤون المكتبات الإلكترونية بإمكانهم الحصول على هذه الخدمة والاستفادة منها .


==خدمة البحث عن مقرر دراسي
الفئة المستهدفة : منسوبي الجامعة فقط 
وفرت العمادة قدر كبير جدا من المقررات الدراسية وذلك عن طريق استخدام أيقونة الخدمات المعلوماتية وتعبئة النموذج المخصص.

==خدمة البحث في الفهرس الآلي للمكتبة
الفئة المستهدفة : للمستفيدين داخل المكتبة وخارجها.
تقدم مكتبة جامعة طيبة خدمة الفهرس الموحد الذي يبحث في جميع الكتب والمقالات والذي يسهل على المستفيد معرفة المصدر المطلوب بسهولة .


==الخدمة المرجعية والرد على الاستفسارات المباشرة وعن طريق الهاتف
هي الخدمة المرجعية المباشرة المكونة من أسئلة المستفيد المرجعية واجابات الاختصاصي عليها ، عبر استخدام وسائل الاتصال الرقمية (البريد الإلكتروني ، المحادثات الفورية ، الاتصال الصوتي ...)

==خدمة البحث في قواعد البيانات الإلكترونية
تحتوي المكتبة على أكثر من 120قاعدة علمية عامة ومتخصصة ،  الصادرة باللغة العربية وغير العربية ،والتي تتيح أحدث ما وصلت إليه الأبحاث والدراسات والرسائل العلمية العالمية .

==خدمة البحث في المكتبة الرقمية السعودية
تعتبر المكتبة الرقمية السعودية بنك معرفي يعمل على مدار 24 ساعة يومياً طيلة أيام الأسبوع لخدمة المستفيدين من منسوبي الجامعات من أعضاء هيئة التدريس والطلبة والمبتعثين ومنسوبي وزارة التعليم للوصول إلى الملايين من مصادر المعلومات الرقمية المتخصصة في المجال العلمي والبحثي."""
  );



  static var internetSearch = Article(
    title: "خدمة البحث في شبكة الإنترنت",
    content: """متاح لزوار وأعضاء المكتبة استخدام الوحدات الطرفية من أجهزة حاسب آلي, و المتوفرة في أرجاء المكتبة و أيضا الأجهزة الموجودة في معمل الحاسب الآلي, والتي بواسطتها يتمكن المستفيد من البحث في المصادر الإلكترونية أو استخدام الإنترنت للأغراض التعليمية الأخرى, حيث تم توصيل جميع أجهزة المكتبة بشبكة الإنترنت، وذلك في المكتبة المركزية وكافة فروعها، بهدف تسهيل البحث للمستفيدين. كما يمكن للمستفيدين إحضار أجهزة الحواسيب الشخصية (المحمولة) واستخدامها لتوافر شبكة الإنترنت في أنحاء المكتبة من خلال شبكة الواي فاي.""",
    imageAssetPaths: [
      "assets/articles/services/other/internetsearch/Picture1.png",
      "assets/articles/services/other/internetsearch/Picture2.png"
    ]
  );

  static var wsa2q = Article(
    title: "خدمه البحث عن الوثائق",
    content: """تقدم هذه الخدمة البحث عن موضوع دراسة معينة لمؤلف معين وإرسالها إلكترونيا للمستفيد .
كيفية طلب الخدمة : 
""",
    imageAssetPaths: [
      "assets/articles/services/other/wtha2q/Picture1.png"
    ]
  );

  static var mstwd3 = Article(
    title: "خدمة مستودع جامعة طيبة الرقمي",
    content: """يحتوي المستودع الرقمي على جميع إنتاج جامعة طيبة المعرفي من رسائل علمية ومؤتمرات وندوات وأبحاث لأعضاء هيئة التدريس وبإمكان جميع زوار البوابة الاستفادة من هذه الخدمات عن طريق زيارة الموقع.
كيفية الدخول على مستودع جامعة طيبة الرقمي:
""",
    imageAssetPaths: [
      "assets/articles/services/other/mswd3/Picture1.png"
    ]
  );

  static var searchResala = Article(
      title: "خدمة البحث عن رسالة علمية",
      content: """ ضمن الخدمات التي تقدمها العمادة خدمة البحث عن رسالة علمية بجامعة طيبة
حيث أن جميع زوار بوابة عمادة شؤون المكتبات الإلكترونية بإمكانهم الحصول على هذه الخدمة والاستفادة منها .
"""
  );

  static var searchMqrr = Article(
      title: "خدمة البحث عن مقرر دراسي",
      content: """الفئة المستهدفة : منسوبي الجامعة فقط 
وفرت العمادة قدر كبير جدا من المقررات الدراسية وذلك عن طريق استخدام أيقونة الخدمات المعلوماتية وتعبئة النموذج المخصص
"""
  );

  static var searchAuto = Article(
      title: "خدمة البحث في الفهرس الآلي للمكتبة",
      content: """الفئة المستهدفة : للمستفيدين داخل المكتبة وخارجها.
تقدم مكتبة جامعة طيبة خدمة الفهرس الموحد الذي يبحث في جميع الكتب والمقالات والذي يسهل على المستفيد معرفة المصدر المطلوب بسهولة .
"""
  );

  static var marg3ya_qas = Article(
      title: "الخدمة المرجعية والرد على الاستفسارات المباشرة وعن طريق الهاتف",
      content: """الخدمة المرجعية والرد على الاستفسارات المباشرة وعن طريق الهاتف
      
      هي الخدمة المرجعية المباشرة المكونة من أسئلة المستفيد المرجعية واجابات الاختصاصي عليها ، عبر استخدام وسائل الاتصال الرقمية (البريد الإلكتروني ، المحادثات الفورية ، الاتصال الصوتي ...)
      """
  );

  static var searchDatabase = Article(
      title: "خدمة البحث في قواعد البيانات الإلكترونية",
      content: """تحتوي المكتبة على أكثر من 120قاعدة علمية عامة ومتخصصة ،  الصادرة باللغة العربية وغير العربية ،والتي تتيح أحدث ما وصلت إليه الأبحاث والدراسات والرسائل العلمية العالمية ."""
  );

  static var searchSaudi = Article(
      title: "خدمة البحث في المكتبة الرقمية السعودية",
      content: """تعتبر المكتبة الرقمية السعودية بنك معرفي يعمل على مدار 24 ساعة يومياً طيلة أيام الأسبوع لخدمة المستفيدين من منسوبي الجامعات من أعضاء هيئة التدريس والطلبة والمبتعثين ومنسوبي وزارة التعليم للوصول إلى الملايين من مصادر المعلومات الرقمية المتخصصة في المجال العلمي والبحثي.""",
      imageAssetPaths: [
        "assets/articles/services/other/sudiSearch/Picture1.png"
      ]
  );

  static var msader = Article(
      title: "خدمة البحث في مصادر المعلومات",
      content: """تقدم مكتبة جامعة طيبة خدمة البحث في مصادر المعلومات , هذه الخدمة تساهم في مساعدة الباحثين عن المصادر في موضوع بحث معين ما على المستفيد إلا أن يضع عنوان البحث وسوف يقوم المختص بإرسال جميع المصادر التي تفيد عنوان البحث  على بريد المستفيد , الفئة المستهدفة من الخدمة جميع مستفيدي جامعة طيبة .

كيفية طلب الخدمة:
""",
      imageAssetPaths: [
        "assets/articles/services/other/msader/Picture1.png"
      ]
  );

  static var hgzMaktab = Article(
      title: "حجز مكتب دراسي",
      content: """تقدم مكتبة جامعة طيبة خدمة البحث في مصادر المعلومات , هذه الخدمة تساهم في مساعدة الباحثين عن المصادر في موضوع بحث معين ما على المستفيد إلا أن يضع عنوان البحث وسوف يقوم المختص بإرسال جميع المصادر التي تفيد عنوان البحث  على بريد المستفيد , الفئة المستهدفة من الخدمة جميع مستفيدي جامعة طيبة .تقدم عمادة شؤون المكتبات خدمة حجز المكاتب الدراسية التي توفر الخصوصية للمستفيد، وهي مصممة بتصميم عصري مريح وبمساحات مختلفة تناسب كافة احتياجات المستفيدين. وهذه الخدمة تتوفر في فرع المكتبة بالمجمع الاكاديمي في السلام وفرع شطر الطلاب في المدينة المنورة فقط. 
كيفية طلب الخدمة : 
""",
      imageAssetPaths: [
        "assets/articles/services/other/hgzMktab/Picture1.png"
      ]
  );


}


class TradServices {
  static var e3ara = Article(
      title: "خدمة الإعارة",
      content:
          """تقدم العمادة خدماتها للباحثين من منسوبي الجامعة ومن خارجها وذلك بعد إصدار العضوية (وهي ان يقوم المسؤول عن خدمة الإعارة بتسجيل المستفيد في قسم الاعارة عن طريق نظام السيمفوني) والتي عن طريقها يمكن الإعارة وتكون الإعارة شخصية  أي لا يسمح لأي شخص استعارة كتب على عضوية شخص آخر ، سواء كان الشخص من داخل الجامعة أو من خارجها.

المواد التي يمكن إعارتها:
    - الكتب التقليدية فقط هي التي تعار أما أجزاء دواوين الشعر, كتب التراجم, المراجع ، الدوريات، النشرات، الرسائل الجامعية، المواد غير الورقية ، المقررات الدراسية ، و الأبحاث العلمية فلا يمكن إعارتها.

الإعارة تشمل الفئات التالية:
طلبة الكلية الجامعية.
الهيئة التدريسية في الجامعة .
الموظفون العاملون في الجامعة .
موظفين ومحاضرين من جامعات أخرى على سبيل التعاون المشترك.

أوقات الدوام:
تقدم المكتبة  لجمهور المُستفيدين من الجامعة وخارجها  خدماتها المكتبية على مدار الأسبوع من السَّاعة الثَّامنة صباحًا وحتَّى السَّاعة السادسة مساء ، عدا أيام الإجازات الرسمية .

أنواع الإعارة:
تقسم الإعارة إلى قسمين هما الإعارة الداخلية و الإعارة الخارجية كالتالي:

أولا: الإعـارة الداخليـة: 
تقـدم هذه الخدمـة لكافة قطاعات الجامعة من طلبة و أساتذة و عاملين و تضم:
الكتب المتداولة والموجودة في المكتبة : تعار هذه الكتب حسب المدة المحددة سابقا قابلة للتجديد إذا لم تكن المادة مطلوبة من قبل شخص آخر.
الكتب الموجودة على رف الحجز : توضع الكتب و غيرها من المواد على رف الحجز بناءً على طلب أعضاء الهيئة التدريسية لارتباط هذه المواد بمساقات معينة.

شروط الإعارة الداخلية للكتب المتداولة:
  يحق لطلبة الجامعة استعارة الكتب خلال الفصل الدراسي في الحالات التالية:
إذا كان الطالب مسجلا رسميا في ذلك الفصل و ورد اسمه في برنامج الإعارة المحوسبة .
إذا كان حائزا على بطاقة عضوية المكتبة و التي يتوجب عليه إبرازها عند استعارة أي كتاب. 

ثانيا : الإعـارة الخارجيـة : 
تقدم هذه الخدمة للمشتركين من خارج الجامعة و الذين يرغبون في الاستعانة بمصادر المكتبة المختلفة ، يخضع المشترك من خارج الجامعة للشروط التالية:
يتم ملء النموذج الخاص بالإعارة الخارجية في قسم الإعارة.
يدفع رسوم تأمين كوديعة تسترد في نهاية مدة الاشتراك.
يحصل على بطاقة اشتراك من المكتبة و يقتصر استعمالها عليه فقط ، حيث لا يجوز أن يستعمل البطاقة غير صاحبها ( اشتراك فردي( يبرز بطاقة الاشتراك عند الرغبة في استعارة أي مادة.
 لا يجوز الاشتراك بالإعارة الخارجية باسم المؤسسات أو الوزارات أو الجمعيات …الخ

 خدمة تمديد الإعارة:
تعتبر هذه الخدمة مكملة لبعض خدمات الإعارة ، حيث أنها تتيح للمستفيد إمكانية تمديد مدة إعارة الكتاب بطريقة إلكترونية دون الحاجة للمجيء للعمادة .
ولعمل خدمة تمديد الإعارة يجب فعل التالي:
1.	الدخول لبوابة عمادة شؤون المكتبات.
2.	ثم الذهاب على أيقونة حسابي .
3.	ومن ثم اختيار الكتاب الذي يراد تمديد فترة إعارته . 
و يتم ذلك بشكل إلكتروني

ثالثا : الحجـز: 
بإمكان المستعير التقدم بالطلب لحجز بعض المواد المتوفرة على الرفوف أو تلك المعارة خارج المكتبة من قبل مستعيرين آخرين. يستمر حجز المواد المطلوبة لمدة يومين بعد إعادتها من قبل المستعيرين في قسم الإعارة، وإذا لم يتقدم أحد لطلب هذه المواد خلال الأيام الثلاثة تعاد مرة أخرى إلى الرفوف.
إذا تلقى المستعير إشعارا بأن مستعيرا آخر طلب حجز المادة التي استعارها من المكتبة ولم يقم بإعادتها ، فقد يتعرض بسبب ذلك لفقدان بعض الامتيازات الخاصة بالإعارة.

مسؤولية المستعيرين تجاه المكتبة:
يتحمل مستعير الكتاب المسؤولية الكاملة عن تأخير و/ أو فقدان أي كتاب ويغرم عليها.
على المستعير إعادة الكتب المعارة عند انتهاء المدة المسموح بها لتجنب الغرامات والعقوبات المترتبة على التأخير.

 ملاحظة: 
المكتبة تقوم بتذكير المستعرين المتأخرين عن إرجاع المواد المعارة لهم عن طريق رسائل الإيميل ورسائل SMS ، وعلى المستعير سرعة الارجاع من تلقاء نفسه.

الغرامات: 
يحق للعمادة فرض غرامات على تأخير وفقدان أو إتلاف المواد على النحو التالي :
الكتب العادية والتي تعار مدة 15 يوم يترتب على تأخيرها غرامة نصف ريال واحد عن كل يوم تأخير.. يحرم الطالب من الإعارة إلا بعد تسديد ما عليه من غرامات مالية.
أما في حالة فقدان الكتاب أو إتلاف بعض أجزائه، يغرم المستعير بدفع ضعف ثمن الكتاب إضافة إلى دفع ثمن الإجراءات الفنية وسعر التجليد.
وفي الحالة السابقة يمكن للمستعير إحضار بدل الكتاب ويحاسب على الإضافات الأخرى.

ملاحظة : 
في جميع الحالات المذكورة أعلاه يحق للمكتبة حرمان المستعير من الإعارة لمدة معينة وفقا لطبيعة الحالة.

تعليمات عامة:
1.	يحق لأعضاء الهيئة التدريسية حجز الكتب المساعدة للمقررات الدراسية وتوضع هذه الكتب تحت بند الكتب المحجوزة على  أن يتم تزويد المكتبة بعناوين المواد التي  يجب وضعها على رف الحجز قبل بداية الفصل بأسبوعين.
2.	في حالة التأخر المتكرر يحرم المستعير من استخدام المكتبة مدة شهر.


ملاحظة:
 في حالات خاصة جدا يحق لمسؤول خدمة الإعارة الطلب من المستعير إعادة الكتب المعارة لديه قبل انتهاء المدة المحددة للإعارة في مدة أقصاها ثلاثة أيام من تاريخ إبلاغه.

مدة الإعارة وعدد الكتب المسموح بإعارتها:
""",
      imageAssetPaths: ["assets/articles/services/e3ara/Picture1.png"]);

  static var er4ad = Article(
      title: "خدمة الارشاد والتوجيه",
      content:
          """وتسعى المكتبة من هذه الخدمة إلى تعريف مستخدميها الحاليين بكيفية استخدام المكتبة وخدماتها, كما تسعى إلى استقطاب اكبر عدد من المستفيدين المتوقعين إلى المكتبة, وذلك باتباع نشاطات ترويجية مختلفة تمثل في حد ذاتها خدمات توجيه وإرشاد.""");

  static var etla3 = Article(
      title: "خدمة الاطلاع الداخلي",
      content: """تتم عن طريق قاعة الكتب المناسبة المخصصة للقراءة والبحث.

الواجبات الملقاة على عاتق رواد المكتبة تجاه المكتبة وموظفيها:

١- الالتزام بالنظام والهدوء داخل المكتبة
٢- الحرص على إبراز البطاقة الجامعيَّة (للطُّلاب والطَّالبات) عند الدُّخول إلى المكتبة.
٣- المحافظة على الكتب وعدم تمزيقها أو الكتابة عليها فالمكتبة ملك لك وللأجيال القادمة.
٤- الالتزام بعدم استعارة الكتب لصالح أشخاص آخرين سواء كانوا من مجتمع الجامعة أو من خارجها .
٥- يمنع منعا باتا التدخين وتناول المأكولات والمشروبات داخل المكتبة.
٦- المطالعة في المكتبة فردية وليست جماعية.
٧- احترام الموظفين والالتزام بأخلاقيات التعامل معهم وخاصة عند توجيه الملاحظات المتعلقة بضبط النظام والهدوء في المكتبة .
""");

  static var hgz_5lwa = Article(
      title: "خدمة حجز خلوة  دراسية",
      content:
          """توفر عمادة شؤون المكتبات خلوات دراسية خاصة لأعضاء هيئة التدريس ,وللحصول على هذه الخدمة يجب التواصل مع العمادة عن  طريق إحدى قنوات التواصل الموجودة ومن ثم الحصول عليها.
تتيح المكتبة في مجمع كليات السلام مكتب دراسي ( وحدة قراءة خاصة) تمنح لمدة عام  دراسي أو فصل دراسي او مدة محددة لمن يطلبها من أعضاء هيئة التدريس وطلبة الدراسات العليا, كما توجد قاعة لإقامة المحاضرات والاجتماعات.""");

  static var tasweer_electronic = Article(
      title: "خدمة التصوير الالكتروني",
      content:
          """خدمة التصوير الالكتروني (الماسح الضوئي) 
ويتم استخدام جهاز   book2net Spirit  وهو آلة تستعمل في إدخال صور ورسومات إلى الحاسوب، حيث يحولها من طبيعتها الرسومية إلى صورة رقمية لكي تناسب طبيعة الحاسوب وحتى يسهل تخزينها داخل ملف واستدعائها وقت الحاجة إليها ويلبي الجهاز جميع المتطلبات بإداء عالي و متقدم لتطبيقات الخدمة الذاتية,  فهو مجهز بأشعة Matrix الحديثة و حامل الكتاب الذكي ,ويوفر book2net Spirit  التكافل الناجح بين المتطلبات الإنتاجية و الجودة و التوفير و الحفاظ على البيئة,  كما أنه يتكيف ديناميكياً مع الإضاءة فهو يسمح بالمسح الضوئي في ضوء النهار فقط , كما يمكن تشغيل إضاءة LED في أي وقت إذا كانت الإضاءة غير ملائمة.
يوفر شريط الإضاءة LED الإضاءة المتجانسة و ذلك بفضل عدسات فرسنل , يسمح  بالمعالجة باللطف لحدود المستندات و هو يعمل على التنظيم الذاتي ليسهل الحركة و هو قابل للتعديل بشكل فردي, وهو مخصص لجميع مستفيدي المكتبة , كما أن عدد الأجهزة في كل فرع يختلف تبعاً لعدد المستفيدين، ففي  كل من المكتبة المركزية ومكتبة السلام  يوجد جهاز واحد، ومكتبه شطر الطلاب يوجد جهازين.
""",
      imageAssetPaths: [
        "assets/articles/services/zatya/Picture1.png",
        "assets/articles/services/zatya/Picture2.png",
      ]);

  static var e3ara_zatya = Article(
    title: "الإعارة الذاتية",
    content: """وذلك عن طريق استخدام جهاز SelfCheck™ System

يساعد M™ SelfCheck™ System المكتبة على زيادة الإعارة وتحسين الإنتاجية وتحسين خدمة العملاء، هذا إلى جانب ميزة الفحص في الوقت الحقيقي مع واجهة سهلة الاستخدام للعملاء والموظفين
نظام M™ SelfCheck™ System هو نظام سلس وسهل الاستخدام يمكنه قراءة بطاقات الطلاب، وشرائط الحماية الممغنطة، والترميز العمودي لمقتنيات المكتبة، ويكون لهذا النظام القدرة على التكامل والربط مع كل من نظام إدارة المكتبة وبوابات الحماية الإلكترونية.
نظام M™ SelfCheck™ System يوفر إمكانية طباعة إيصال بالاستعارة أو إرسال التفاصيل إلى بريد المستفيد, كما يقدم النظام لفريق المكتبة تقارير إحصائية بمعدلات واتجاهات استخدام الأوعية " وهو جهاز مخصص لجميع المستفيدين من مكتبة الجامعة, كما أنه تختلف عدد الأجهزة في كل فرع تبعاً لعدد المستفيدين، ففي كل من المكتبة المركزية ومكتبة السلام ومكتبة شطر الطلاب يوجد جهاز واحد فقط .
""",
    imageAssetPaths: [
      "assets/articles/services/zatya/Picture3.png",
      "assets/articles/services/zatya/Picture4.png",
      "assets/articles/services/zatya/Picture5.png",
    ]
  );

  static var tsweer_warq = Article(
      title: "التصوير الورقي",
      content: """ويتم استخدام جهاز  Canon image RUNNER ADVANCE 400i
طابعة imageRUNNER ADVANCE صغيرة الحجم,  وهي مصممة بكرت مسبق الدفع وهو عبارة عن كرت به مبلغ مالي يتم شراؤه بمقابل مالي من المكتبة، ومن الممكن أن تقوم المكتبة بتوفيره للطلبة ولم تحدد المكتبة السياسة المتبعة بعد كما أنه مصمم لمجموعات العمل النشطة, مع معالجة ذكية للمستندات لتحسين مهام سير العمل وحماية المعلومات السرية وهو مخصص لجميع مستفيدي المكتبة، وتختلف عدد الأجهزة في كل فرع من فروع المكتبة حسب عدد المستفيدين من المكتبة، ففي المكتبة المركزية توجد 3 أجهزة  وفي مكتبه السلام  يوجد جهازين وفي مكتبه شطر الطلاب يوجد 3 أجهزة .
المزايا:-
إخراج فعال أحادي اللون يبلغ 40 صفحة في الدقيقة بحجم A4
تصميم موفر للمساحة
شاشة لمس ملونة قابلة للتخصيص بدقة SVGA قابلة للإمالة مقاس 17.8 سم
سعة حتى 2.300 صفحة
تصوير قوي وتحكم في التكلفة وتكامل
خيارات أمان متقدمة
كفاءة طاقة رائدة ضمن فئتها
إخراج وتصوير فعال
توفير الحماية والأمان للمستندات 
تحافظ على البيئة 

تعالج الطابعة imageRUNNER ADVANCE 400i أحمال عمل مجموعات العمل والأقسام النشطة, بفضل التصميم القوي وسعة الورق البالغة 2.300 ورقة وسرعة إخراج سريعة تبلغ 40 ورقة في الدقيقة. توفر الشاشة الملونة الكبيرة التي تعمل باللمس والقوائم البسيطة ومصادقة المستخدم الذكية تجربة مخصصة فريدة وتشغيلاً بدون مجهود. تتم معالجة المستندات بدون مجهود باستخدام المسح الضوئي الملون السريع والتحويل إلى مجموعة من التنسيقات. يمكن تتبع المستندات وتأمينها وتوقيعها وأرشفتها وإرسالها إلى مجموعة من الوجهات. لتحسين العملية بالكامل.""",
      imageAssetPaths: [
        "assets/articles/services/tsweer/Picture1.png",
        "assets/articles/services/tsweer/Picture2.png",
        "assets/articles/services/tsweer/Picture3.png",
      ]);


  static var zatya = CombinedArticle("الخدمات الذاتية للمكتبة", [
    tasweer_electronic,
    tsweer_warq,
    e3ara_zatya
  ],
  content: "الخدمة الذاتية في المكتبات هي الخدمات القائمة على توفير التقنيات التي تؤدي إلى اعتماد المستفيد على نفسه في الحصول على الخدمة.");
}
