import 'package:anerny/ui/auto_index_page.dart';
import 'package:anerny/ui/info_bank.dart';
import 'package:anerny/ui/intro.dart';
import 'package:anerny/ui/last_news.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/services_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/material.dart';

class Ar4dnePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);
    return BaseScaffold(
      body: CustomPaint(
        painter: ShapesPainter(),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: eqH(1),
                width: eqW(14),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        shape: BoxShape.circle,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Image.asset(
                              "assets/home_ar4dny.png",
                              width: 50,
                            ),
                            Text("ارشدني",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22,color: Colors.black),)
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: eqH(12),
                left: 0,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (x)=>AutoIndexPage()));
                      },
                      child: Column(
                        children: <Widget>[
                          Text("الفهرس الآلي",style: TextStyle(fontSize: 22,color: Color(0xff303742)),),
                          Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(image: AssetImage("assets/yellow_ring.png"))
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(32.0),
                              child: Image.asset("assets/auto_index.png",width: 40,),
                            ),
                          )
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (x)=>ServicesPage()));
                      },
                      child: Column(
                        children: <Widget>[
                          Text("خدمات المكتبة",style: TextStyle(fontSize: 22,color: Color(0xff303742)),),
                          Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(image: AssetImage("assets/gray_ring.png"))
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(32.0),
                              child: Image.asset("assets/services.png",width: 40,),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}


class HomeButton extends StatelessWidget {
  final double x;
  final double y;
  final String imagePath;
  final String text;
  final Function onTap;

  HomeButton({this.x, this.y, this.imagePath, this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);
    return Positioned(
      top: y,
      left: x,
      width: eqW(2.5),
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                imagePath,
                width: eqW(1.8),
                height: eqH(1.8),
              ),
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);

    final paint = Paint();
    final path = Path();

    path.reset();
    paint.color = Color(0xff9BC8CE);
    path.moveTo(eqW(0), eqH(0));
    path.lineTo(eqW(14), eqH(0));
    path.lineTo(eqW(14), eqH(4.5));
    path.lineTo(eqW(0), eqH(10.5));
    canvas.drawPath(path, paint);

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
