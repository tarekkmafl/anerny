


import 'package:anerny/data/data/services.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/electronic_services_other1.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class ElectronicServicesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Center(
            child: RoundedButton(
              width: MediaQuery.of(context).size.width * 0.8,
              text: Text(
                "الخدمات الإلكترونية",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: OneSideRoundedButton(
                            onTap: ()=>pushArticleScreen(context,ElectronicServices.marg3ya),
                            right: true,
                            text: Text("الخدمة المرجعية الرقمية",style: TextStyle(color: Colors.white,fontSize: 14),textAlign: TextAlign.center,),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.dawrat),
                        text: Text("طلب دورات تدريبيه",style: TextStyle(color: Colors.white,fontSize: 16,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.tlbZyara),
                        text: Text("طلب زيارة المكتبة",style: TextStyle(color: Colors.white,fontSize: 16,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.hgzM3ml),
                        text: Text("حجز معمل تدريب",style: TextStyle(color: Colors.white,fontSize: 16,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (_)=>ElectronicServicesOther1Page()));
                        },
                        text: Text("خدمات\nآخرى",style: TextStyle(color: Colors.white,fontSize: 16,),textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerRight,
                        child: OneSideRoundedButton(
                          right: false,
                          onTap: ()=>pushArticleScreen(context,ElectronicServices.hmm),
                          text: Text("خدمات ذوي الهمم",style: TextStyle(color: Colors.white,fontSize: 16,),textAlign: TextAlign.center,),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: OneSideRoundedButton(
                          right: false,
                          onTap: ()=>pushArticleScreen(context,ElectronicServices.bahsBrith),
                          text: Text("البحث في المكتبة البريطانيه",style: TextStyle(color: Colors.white,fontSize: 16,),textAlign: TextAlign.center,),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          )
        ],
      ),
    );
  }
}


class CircleButton extends StatelessWidget {

  final Widget text;
  final Function onTap;

  CircleButton({this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white, width: 2),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Align(alignment: Alignment.center,child: text),
        ),
      ),
    );
  }
}


class OneSideRoundedButton extends StatelessWidget {

  final bool right;
  final Widget text;
  final Function onTap;
  final double width;

  OneSideRoundedButton({this.text, this.onTap, this.width, this.right});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: MediaQuery.of(context).size.width/2,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 2),
          borderRadius: !right ? BorderRadius.only(topLeft: Radius.circular(50),bottomLeft: Radius.circular(50)) :  BorderRadius.only(topRight: Radius.circular(50),bottomRight: Radius.circular(50)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Align(alignment: Alignment.center,child: text),
        ),
      ),
    );
  }
}
