


import 'package:anerny/data/data/services.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/electronic_services.dart';
import 'package:anerny/ui/electronic_services_other2.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class ElectronicServicesOther1Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Center(
            child: RoundedButton(
              width: MediaQuery.of(context).size.width * 0.8,
              text: Text(
                "الخدمات الإلكترونية",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: OneSideRoundedButton(
                            onTap: ()=>pushArticleScreen(context,ElectronicServices.marg3ya_qas),
                            right: true,
                            text: Text("الخدمة المرجعية والرد على الإستفسارات المباشرة وعن طريق الهاتف",style: TextStyle(color: Colors.white,fontSize: 12),textAlign: TextAlign.center,),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.searchDatabase),
                        text: Text("خدمة البحث في قواعد البيانات الإلكترونية",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.searchMqrr),
                        text: Text("خدمة البحث عن مقرر دراسي",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.searchAuto),
                        text: Text("خدمة البحث في الفهرس الآلي للمكتبة",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (_)=>ElectronicServicesOther2Page()));
                        },
                        text: Text("خدمات\nآخرى",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerRight,
                        child: OneSideRoundedButton(
                          right: false,
                          onTap: ()=>pushArticleScreen(context,ElectronicServices.internetSearch),
                          text: Text("خدمة البحث في شبكة الإنترنت",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: OneSideRoundedButton(
                          right: false,
                          onTap: ()=>pushArticleScreen(context,ElectronicServices.searchSaudi),
                          text: Text("خدمة البحث في المكتبة الرقمية السعودية",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          )
        ],
      ),
    );
  }
}
