import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';

class VideoPlayerPage extends StatefulWidget {
  final String videoAssetsPath;

  const VideoPlayerPage({Key key, @required this.videoAssetsPath})
      : super(key: key);

  @override
  VideoPlayerPageState createState() {
    return new VideoPlayerPageState();
  }
}

class VideoPlayerPageState extends State<VideoPlayerPage> {
  ChewieController _controller;

  bool isInitialized = false;

  @override
  void initState() {
    super.initState();

    Future.sync(() async {
      var baseController = VideoPlayerController.asset(widget.videoAssetsPath);
      await baseController.initialize();
      isInitialized = true;
      _controller = ChewieController(
        videoPlayerController: baseController,
        aspectRatio: baseController.value.aspectRatio,
        autoPlay: true,
      );
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Center(
        child: isInitialized
            ? Chewie(
                controller: _controller,
              )
            : Container(),
      ),
    );
  }

  @override
  void dispose() {
    _controller.videoPlayerController.dispose();
    _controller.dispose();
    super.dispose();
  }
}
