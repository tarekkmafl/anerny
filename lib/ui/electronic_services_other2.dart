


import 'package:anerny/data/data/services.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/electronic_services.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class ElectronicServicesOther2Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Center(
            child: RoundedButton(
              width: MediaQuery.of(context).size.width * 0.8,
              text: Text(
                "الخدمات الإلكترونية",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: OneSideRoundedButton(
                            onTap: ()=>pushArticleScreen(context,ElectronicServices.msader),
                            right: true,
                            text: Text("خدمة البحث في مصادر المعلومات",style: TextStyle(color: Colors.white,fontSize: 14),textAlign: TextAlign.center,),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.mstwd3),
                        text: Text("خدمة مستودع طيبة الرقمي",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.hgzMaktab),
                        text: Text("حجز مكتب دراسي",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                      ),
                      CircleButton(
                        onTap: ()=>pushArticleScreen(context,ElectronicServices.wsa2q),
                        text: Text("خدمة البحث عن الوثائق",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerRight,
                        child: OneSideRoundedButton(
                          right: false,
                          onTap: ()=>pushArticleScreen(context,ElectronicServices.searchResala),
                          text: Text("خدمة البحث عن رسالة علمية",style: TextStyle(color: Colors.white,fontSize: 12,),textAlign: TextAlign.center,),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(width: 10,height: 10,),
                      ),
                    ],
                  ),
                ),
              ],
            )
          )
        ],
      ),
    );
  }
}


