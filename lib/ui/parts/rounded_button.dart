import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final Widget text;
  final Function onTap;
  final double width;
  final Color borrderColor;

  RoundedButton({this.text, this.onTap, this.width, this.borrderColor = Colors.white,});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width,
        decoration: BoxDecoration(
          border: Border.all(color: borrderColor, width: 2),
          borderRadius: BorderRadius.circular(50),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Align(alignment: Alignment.center,child: text),
        ),
      ),
    );
  }
}
