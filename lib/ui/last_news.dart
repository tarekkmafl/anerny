




import 'package:anerny/data/data.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';


class LastNewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.only(bottom: 80),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 80,
              ),
              Center(
                child: RoundedButton(
                  width: MediaQuery.of(context).size.width * 0.8,
                  text: Text(
                    "أخر الأخبار",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Column(
                children: lastNews,
              )
            ],
          ),
        ),
      ),
    );
  }
}



class LastNewsItem extends StatelessWidget {
  final String text;
  final String imagePath;

  const LastNewsItem({Key key, this.text, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        color: Color(0xff86aab1),
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Text(text,textDirection: TextDirection.rtl,),
              imagePath == null ? Container(width: 0,height: 0,) : Image.asset(imagePath)
            ],
          ),
        ),
      ),
    );
  }
}
