import 'package:anerny/data/data/contact.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ContactLibraryPage extends StatelessWidget {
  final corousol = CarouselSlider(
    autoPlay: true,
    enlargeCenterPage: true,
    height: 200.0,
    aspectRatio: 3 / 2,
    items: Contact.slideImagesPath.map((i) {
      return Builder(
        builder: (BuildContext context) {
          return Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                child: Image.asset(
                  i,
                  fit: BoxFit.cover,
                  width: 1000,
                ),
              ));
        },
      );
    }).toList(),
  );

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Center(
            child: RoundedButton(
              width: MediaQuery.of(context).size.width / 2,
              text: Text(
                "التواصل مع المكتبة",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 22,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Stack(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: corousol,
                            ),
                            Align(
                                alignment: Alignment.centerLeft,
                                child: InkWell(
                                  onTap: () {
                                    corousol.previousPage(
                                        duration: Duration(milliseconds: 200),
                                        curve: Curves.linear);
                                  },
                                  child: Image.asset(
                                    "assets/prev.png",
                                    width: 35,
                                  ),
                                )),
                            Align(
                                alignment: Alignment.centerRight,
                                child: InkWell(
                                  onTap: () {
                                    corousol.nextPage(
                                        duration: Duration(milliseconds: 200),
                                        curve: Curves.linear);
                                  },
                                  child: Image.asset(
                                    "assets/next.png",
                                    width: 35,
                                  ),
                                )),
                          ],
                        ),
                      ),
                    )
                  ]),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                onTap: () {
                  pushArticleScreen(context, Contact.locations);
                },
                child: Image.asset(
                  "assets/location.png",
                  width: 80,
                ),
              ),
              InkWell(
                onTap: () {
                  pushArticleScreen(context, Contact.accounts);
                },
                child: Image.asset(
                  "assets/accounts.png",
                  width: 80,
                ),
              ),
              InkWell(
                onTap: (){
                  pushArticleScreen(context,Contact.times);
                },
                child: Image.asset(
                  "assets/times.png",
                  width: 80,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 50,
          )
        ],
      ),
    );
  }
}
