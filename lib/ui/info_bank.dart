import 'package:anerny/data/data/info_bank.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoBankPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 30,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      shape: BoxShape.circle,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Image.asset(
                            "assets/home_bank.png",
                            width: 50,
                          ),
                          Text("بنوك المعلومات",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22,color: Colors.black),)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              ArticleWidget(InfoBank.details),
              SizedBox(height: 30,),
              RoundedButton(
                borrderColor: Color(0xfff0e984),
                text: Text("للدخول على بنوك المعلومات اضغط هنا",style: TextStyle(color: Colors.white,fontSize: 16),textAlign: TextAlign.center,),
                onTap: (){
                  launch("http://elibrary.taibahu.edu.sa/resources");
                },
                width: MediaQuery.of(context).size.width*0.7,
              ),
              SizedBox(height: 70,),
            ],
          ),
        ),
      ),
    );
  }
}
