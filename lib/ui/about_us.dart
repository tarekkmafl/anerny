


import 'package:anerny/data/data/about.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class AboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Center(
            child: RoundedButton(
              width: MediaQuery.of(context).size.width/2,
              text: Text(
                "عن أنرني",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerRight,
                  child: OneSideRoundedButton(
                    right: false,
                    onTap: (){
                      pushArticleScreen(context,About.ro2ya);
                    },
                    text: Text("رؤيتنا",style: TextStyle(color: Colors.white,fontSize: 22),),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: OneSideRoundedButton(
                    right: true,
                    onTap: (){
                      pushArticleScreen(context,About.resala);
                    },
                    text: Text("رسالتنا",style: TextStyle(color: Colors.white,fontSize: 22)),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: OneSideRoundedButton(
                    right: false,
                    onTap: (){
                      pushArticleScreen(context,About.ahdafna);
                    },
                    text: Text("أهدافنا",style: TextStyle(color: Colors.white,fontSize: 22)),
                  ),
                ),
                Container(),
              ],
            ),
          )
        ],
      ),
    );
  }
}


class OneSideRoundedButton extends StatelessWidget {

  final bool right;
  final Widget text;
  final Function onTap;
  final double width;

  OneSideRoundedButton({this.text, this.onTap, this.width, this.right});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: MediaQuery.of(context).size.width/2,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 2),
          borderRadius: !right ? BorderRadius.only(topLeft: Radius.circular(20),bottomLeft: Radius.circular(20)) :  BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Align(alignment: Alignment.center,child: text),
        ),
      ),
    );
  }
}
