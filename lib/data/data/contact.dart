




import 'package:anerny/data/data_struct.dart';

class Contact{

  static List<String> get slideImagesPath {
    List<String> list = [];
    for(int i=1;i<=13;i++){
      list.add("assets/articles/contact/slide/Picture$i.png");
    }
    return list;
  }

  static var times = Article(
      title: "أوقات الدوام",
      content: """أوقات الدوام شطر السلام و المكتبة المركزية : 
الطالبات : يبدأ من الساعة الثامنة صباحأ حتى السادسة مساء .
 الطلاب : يبدأ من الثامنة صباحآ حتى التاسعة مساء ."""
  );


  static var accounts = Article(
    title: "الحسابات على مواقع التواصل الاجتماعي",
    content: """الحسابات على مواقع التواصل الاجتماعي 
حساب عمادة شؤون المكتبات على تويتر :   Library_taibah

البريد الإلكتروني لعمادة شؤون المكتبات : library@taibah.edu.sa

أرقام التواصل مع المكتبة

هاتف عمادة شؤون المكتبات : 00966148472569
المكتبة المركزية قسم الطالبات ت :  04/8460008 تحويلة : 3841
مكتبة مجمع السلام قسم الطالبات ت : 04/8460008 تحويلة : 3841
"""
  );

  static var locations = Article(
    title:"المواقع والفروع",
    content: "تتضمن شبكة مكتبات جامعة طيبة على 16 مكتبة وهي كالاتي :",
    imageAssetPaths: [
      "assets/articles/contact/locations/locations.png"
    ]
  );

}