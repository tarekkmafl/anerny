import 'package:flutter/material.dart';

class Article {
  final String title;
  final String content;
  final List<String> imageAssetPaths;

  Article({this.title, this.content, this.imageAssetPaths});
}

class CombinedArticle {
  final String title;
  final String content;
  final List<Article> articles;

  CombinedArticle(this.title, this.articles, {this.content});
}

pushArticleScreen(BuildContext context, dynamic a) {
  if (a is Article) Navigator.push(context, MaterialPageRoute(builder: (_) => ArticleScreen(a)));
  if (a is CombinedArticle) Navigator.push(context, MaterialPageRoute(builder: (_) => CombinedArticleScreen(a)));
}

class ArticleScreen extends StatelessWidget {
  final Article article;

  ArticleScreen(this.article);

  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];

    if (article.content != null)
      children.add(
        Padding(
          padding: const EdgeInsets.all(32.0),
          child: Text(
            article.content,
            textDirection: TextDirection.rtl,
            style: TextStyle(color: Colors.white),
          ),
        ),
      );

    if (article.imageAssetPaths != null) {
      article.imageAssetPaths.forEach((imgPath) {
        children.add(Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Image.asset(imgPath),
        ));
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text(
          article.title ?? "",
          style: TextStyle(fontWeight: FontWeight.bold),
        )),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 32),
        child: ArticleWidget(article),
      ),
    );
  }
}

class CombinedArticleScreen extends StatelessWidget {
  final CombinedArticle articles;

  CombinedArticleScreen(this.articles);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text(
          articles.title ?? "",
          style: TextStyle(fontWeight: FontWeight.bold),
        )),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 32),
        child: CombinedArticleWidget(articles),
      ),
    );
  }
}

class CombinedArticleWidget extends StatelessWidget {
  final CombinedArticle articles;

  CombinedArticleWidget(this.articles);

  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];

    if (articles.content != null)
      children.add(
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            articles.content,
            style: TextStyle(color: Colors.white, fontSize: 16),
            textAlign: TextAlign.right,
            textDirection: TextDirection.rtl,
          ),
        ),
      );

    children.addAll(articles.articles
        .map((m) => ArticleWidget(
              m,
              withTitle: true,
            ))
        .toList());

    return Column(
      children: children,
    );
  }
}

class ArticleWidget extends StatelessWidget {
  final Article article;
  final bool withTitle;

  ArticleWidget(this.article, {this.withTitle});

  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];

    if (withTitle != null) {
      children.add(Padding(
        padding: EdgeInsets.all(32),
        child: Center(
            child: Text(
          article.title,
          style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          textDirection: TextDirection.rtl,
        )),
      ));
    }

    if (article.content != null)
      children.add(
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            article.content,
            style: TextStyle(color: Colors.white, fontSize: 16),
            textAlign: TextAlign.right,
            textDirection: TextDirection.rtl,
          ),
        ),
      );

    if (article.imageAssetPaths != null) {
      article.imageAssetPaths.forEach((imgPath) {
        children.add(Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Image.asset(imgPath),
        ));
      });
    }

    return Column(
      children: children,
    );
  }
}
