



import 'package:anerny/ui/about_emada_page.dart';
import 'package:anerny/ui/contact_lib_page.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:flutter/material.dart';

class EmadaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset("assets/emada.png"),
          ),
          Center(
            child: Container(
              height: MediaQuery.of(context).size.width,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (_)=>ContactLibraryPage()));
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(),
                  ),
                  Expanded(
                    flex: 6,
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (_)=>AboutEmadaPage()));
                      },
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

