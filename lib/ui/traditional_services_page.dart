import 'package:anerny/data/data/services.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';

class TraditionalServicesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Center(
            child: RoundedButton(
              width: MediaQuery.of(context).size.width * 0.8,
              text: Text(
                "الخدمات التقليدية",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("assets/services_list_bg.png"),fit: BoxFit.fill),
                ),
                child: GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio: 2.5/2,
                  crossAxisSpacing: 20,
                  children: <Widget>[
                    RoundedDottedButton(
                      onTap: ()=>pushArticleScreen(context,TradServices.e3ara),
                      text: Text("الإعارة",style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                    ),
                    RoundedDottedButton(
                      onTap: ()=>pushArticleScreen(context,TradServices.er4ad),
                      text: Text("الإرشاد والتوجيه",style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                    ),
                    RoundedDottedButton(
                      onTap: ()=>pushArticleScreen(context,TradServices.zatya),
                      text: Text("خدمات ذاتية",style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                    ),
                    RoundedDottedButton(
                      onTap: ()=>pushArticleScreen(context,TradServices.etla3),
                      text: Text("الإطلاع الداخلي",style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                    ),
                    RoundedDottedButton(
                      onTap: ()=>pushArticleScreen(context,TradServices.hgz_5lwa),
                      text: Text("حجز خلوه دراسية",style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class RoundedDottedButton extends StatelessWidget {
  final Widget text;
  final Color bgColor;
  final Function onTap;
  final Color borrderColor;

  RoundedDottedButton(
      {this.text,
      this.onTap,
      this.borrderColor = Colors.black,
      this.bgColor = const Color(0xffF3EB96)});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("assets/services_bg.png")),
            border: Border.all(color: borrderColor, width: 2,style: BorderStyle.none),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(alignment: Alignment.center, child: text),
          ),
        ),
      ),
    );
  }
}
