


import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;

class LinksPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Center(
            child: RoundedButton(
              width: MediaQuery.of(context).size.width/2,
              text: Text(
                "روابط تهمك",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerRight,
                  child: OneSideRoundedButton(
                    onTap: (){
                      launch("https://www.taibahu.edu.sa/");
                    },
                    right: false,
                    text: Text("جامعة طيبة",style: TextStyle(color: Colors.white),),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: OneSideRoundedButton(
                    onTap: (){
                      launch("https://www.taibahu.edu.sa/Pages/AR/Sector/SectorPage.aspx?ID=47");
                    },
                    right: true,
                    text: Text("عمادة شئون المكتبات بجامعة طيبة",style: TextStyle(color: Colors.white)),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: OneSideRoundedButton(
                    onTap: (){
                      launch("http://elibrary.taibahu.edu.sa/Taibah");
                    },
                    right: false,
                    text: Text("المكتبة الرقمية",style: TextStyle(color: Colors.white)),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: OneSideRoundedButton(
                    onTap: (){
                      if(Platform.isAndroid) {
                        launch("https://play.google.com/store/apps/details?id=com.techknowledge.deepknowledge");
                      }else if (Platform.isIOS){
                        launch("https://itunes.apple.com/us/app/deepknowledge/id571246689?ls=1&mt=8");
                      }
                    },
                    right: true,
                    text: Text("تطبيق DeepKnowledge",textDirection: TextDirection.rtl,style: TextStyle(color: Colors.white)),
                  ),
                ),
                Container(),
              ],
            ),
          )
        ],
      ),
    );
  }
}


class OneSideRoundedButton extends StatelessWidget {

  final bool right;
  final Widget text;
  final Function onTap;
  final double width;

  OneSideRoundedButton({this.text, this.onTap, this.width, this.right});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: MediaQuery.of(context).size.width/2,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 2),
          borderRadius: !right ? BorderRadius.only(topLeft: Radius.circular(20),bottomLeft: Radius.circular(20)) :  BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Align(alignment: Alignment.center,child: text),
        ),
      ),
    );
  }
}
