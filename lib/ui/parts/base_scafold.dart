import 'package:anerny/ui/about_us.dart';
import 'package:anerny/ui/contact_us.dart';
import 'package:anerny/ui/home.dart';
import 'package:anerny/ui/links_page.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';

class BaseScaffold extends StatefulWidget {
  final bool showBottomButtons;
  final Widget body;

  const BaseScaffold({Key key, this.body, this.showBottomButtons = true})
      : super(key: key);

  @override
  BaseScaffoldState createState() {
    return new BaseScaffoldState();
  }
}

class BaseScaffoldState extends State<BaseScaffold> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          widget.body,
          SafeArea(
            child: Align(
              alignment: Alignment.topRight,
              child: SizedBox(
                  width: 60,
                  child: InkWell(
                      onTap: () {
                        print("J");
                        _scaffoldKey.currentState.openEndDrawer();
                      },
                      child: IconButton(
                          icon: Icon(
                            Icons.menu,
                            color: Colors.white,
                            size: 50,
                          ),
                          onPressed: null))),
            ),
          ),
          widget.showBottomButtons
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Navigator.canPop(context)
                            ? InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Image.asset(
                                  "assets/back_btn.png",
                                  width: 50,
                                ))
                            : Container(
                                width: 0,
                                height: 0,
                              ),
                        Spacer(),
                        InkWell(
                          onTap: () {
                            while (Navigator.pop(context)){}
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(builder: (x) => HomePage()));
                          },
                          child: Image.asset(
                            "assets/home_btn.png",
                            width: 50,
                          ),
                        )
                      ],
                    ),
                  ),
                )
              : Container(
                  width: 0,
                  height: 0,
                )
        ],
      ),
      endDrawer: Drawer(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    shape: BoxShape.circle,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: Image.asset(
                      "assets/logo.png",
                      width: 120,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 60,
            ),
            DrawerButton(
              text: "الرئيسية",
              onTap: () async {
                while (Navigator.pop(context)){}
                await Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (x) => HomePage()));
              },
            ),
            DrawerButton(
              text: "عن أنرني",
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (x) => AboutUsPage()));
              },
            ),
            DrawerButton(
              text: "أتصل بنا",
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (x) => ContactUsPage()));
              },
            ),
            DrawerButton(
              text: "روابط تهمك",
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (x) => LinksPage()));
              },
            )
          ],
        ),
      ),
    );
  }
}

class DrawerButton extends StatelessWidget {
  final String text;
  final Function onTap;

  DrawerButton({this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: RoundedButton(
        text: Text(
          text,
          style: TextStyle(
              fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        onTap: onTap,
      ),
    );
  }
}
