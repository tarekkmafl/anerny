import 'package:anerny/data/data/emada.dart';
import 'package:anerny/data/data_struct.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:flutter/material.dart';

class AboutEmadaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        InkWell(
                            onTap: () {
                              pushArticleScreen(context, Emada.na4aa);
                            },
                            child: Image.asset("assets/n4at_elmaktaab.png")),
                        InkWell(
                            onTap: () {
                              pushArticleScreen(context, Emada.mansobe);
                            },
                            child: Image.asset("assets/masobe_emada.png")),
                        SizedBox(
                          height: 30,
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 30,
                        ),
                        InkWell(
                            onTap: () {
                              pushArticleScreen(context, Emada.ro2ya);
                            },
                            child: Image.asset("assets/ro2ya_resala.png")),
                        InkWell(
                            onTap: () {
                              pushArticleScreen(context, Emada.hykal);
                            },
                            child: Image.asset("assets/hykal.png"))
                      ],
                    ),
                  )
                ],
              ),
              Image.asset(
                "assets/about_emada.png",
                width: 80,
              )
            ],
          ),
        ),
      ),
    );
  }
}
