


import 'dart:convert';

import 'package:anerny/data/data.dart';
import 'package:anerny/ui/home.dart';
import 'package:anerny/ui/intro.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 2)).then((x) async{
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if(prefs.getKeys().contains("intro") == false || prefs.getBool("intro") == false){
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (x)=>IntroPage()));
      }else{
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (x)=>HomePage()));
      }
    });
    return Scaffold(
      body: Container(
        child: Center(
          child: SizedBox(
              child: Image.asset("assets/logo.png"),width: 300,height: 300,),
        ),
      ),
    );
  }
}
