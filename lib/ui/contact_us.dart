import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/parts/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUsPage extends StatefulWidget {
  @override
  ContactUsPageState createState() {
    return new ContactUsPageState();
  }
}

class ContactUsPageState extends State<ContactUsPage> {
  var suggestionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: Builder(
        builder: (context) => SingleChildScrollView(
              padding: EdgeInsets.only(bottom: 80),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 80,
                  ),
                  Center(
                    child: RoundedButton(
                      width: MediaQuery.of(context).size.width / 2,
                      text: Text(
                        "اتصل بنا",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 28,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 80,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            launch("http://twitter.com/anrunii");
                          },
                          child: Image.asset(
                            "assets/twitter.png",
                            width: 65,
                          )),
                      InkWell(
                          onTap: () {
                            launch("http://instagram.com/anruni1");
                          },
                          child: Image.asset(
                            "assets/instgram.png",
                            width: 65,
                          )),
                      InkWell(
                          onTap: () {
                            launch("mailto:anruni@outlook.sa");
                          },
                          child: Image.asset(
                            "assets/email.png",
                            width: 65,
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: RoundedButton(
                      width: MediaQuery.of(context).size.width / 2,
                      text: Text(
                        "إقتراحات",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 3),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          controller: suggestionController,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            hintText: "أكتب أقتراحك هنا",
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: RoundedButton(
                      width: MediaQuery.of(context).size.width / 2,
                      onTap: () {
                        suggestionController.clear();
                        showDialog(
                            context: context,
                            builder: (context) => Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: AlertDialog(
                                    title: Text("تم الإرسال"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text("حسنا"),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                ));
                      },
                      text: Text(
                        "إرسال",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
      ),
    );
  }
}
