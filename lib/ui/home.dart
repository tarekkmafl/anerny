import 'package:anerny/ui/ar4dne.dart';
import 'package:anerny/ui/emada_page.dart';
import 'package:anerny/ui/info_bank.dart';
import 'package:anerny/ui/intro.dart';
import 'package:anerny/ui/last_news.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);
    return BaseScaffold(
      showBottomButtons: false,
      body: CustomPaint(
        painter: ShapesPainter(),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: eqH(2),
                width: eqW(14),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      shape: BoxShape.circle,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(32.0),
                      child: Image.asset(
                        "assets/logo.png",
                        width: 120,
                      ),
                    ),
                  ),
                ),
              ),
              HomeButton(
                x: eqW(2),
                y: eqH(10),
                imagePath: "assets/home_3mada.png",
                text: "عن عمادة شؤون المكتبات",
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (x)=>EmadaPage()));
                },
              ),
              HomeButton(
                x: eqW(0),
                y: eqH(14),
                imagePath: "assets/home_bank.png",
                text: "بنوك المعلومات",
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (x)=>InfoBankPage()));
                },
              ),
              HomeButton(
                x: eqW(9.2),
                y: eqH(15.5),
                imagePath: "assets/home_bank.png",
                text: "أخر الأخبار",
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (x)=>LastNewsPage()));
                },
              ),
              HomeButton(
                x: eqW(10),
                y: eqH(10.5),
                imagePath: "assets/home_ar4dny.png",
                text: "أرشدني",
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (x)=>Ar4dnePage()));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class HomeButton extends StatelessWidget {
  final double x;
  final double y;
  final String imagePath;
  final String text;
  final Function onTap;

  HomeButton({this.x, this.y, this.imagePath, this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);
    return Positioned(
      top: y,
      left: x,
      width: eqW(2.5),
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                imagePath,
                width: eqW(1.8),
                height: eqH(1.8),
              ),
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);

    final paint = Paint();
    final path = Path();

    path.reset();
    paint.color = Color(0xff8bbdc3);
    path.moveTo(eqW(0), eqH(4));
    path.lineTo(eqW(10.5), eqH(14.5));
    path.lineTo(eqW(7), eqH(18));
    path.lineTo(eqW(0), eqH(11));
    canvas.drawPath(path, paint);

    path.reset();
    paint.color = Color(0xff2c303D);
    path.moveTo(eqW(14), eqH(4));
    path.lineTo(eqW(14), eqH(18));
    path.lineTo(eqW(7), eqH(11));
    canvas.drawPath(path, paint);

    path.reset();
    paint.color = Color(0xffb8a11e);
    path.moveTo(eqW(14), eqH(18));
    path.lineTo(eqW(7), eqH(18));
    path.lineTo(eqW(10.5), eqH(14.5));
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
