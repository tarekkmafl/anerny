import 'package:anerny/ui/electronic_services.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/traditional_services_page.dart';
import 'package:flutter/material.dart';

class ServicesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Image.asset(
                    "assets/services_ii.png",
                    width: 150,
                  )),
            ),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.topCenter,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (x)=>TraditionalServicesPage()));
                      },
                      child: Image.asset(
                        "assets/trad_serv.png",
                        width: 250,
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (x)=>ElectronicServicesPage()));
                      },
                      child: Image.asset(
                        "assets/ekec_servi.png",
                        width: 250,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
