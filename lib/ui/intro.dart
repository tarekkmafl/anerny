


import 'package:anerny/ui/electronic_services.dart';
import 'package:anerny/ui/home.dart';
import 'package:anerny/ui/traditional_services_page.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

class IntroPage extends StatelessWidget {


  _navigateToHome(BuildContext context) async {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (x)=>HomePage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(child: Image.asset("assets/intro_bg.png")),
          Column(
            children: <Widget>[
              Align(
                alignment: AlignmentDirectional.center,
                child: Padding(
                  padding: const EdgeInsets.all(32.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/logo.png",width: 200,height: 200,),
                      RichText(text: TextSpan(style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),children: [
                        TextSpan(text: "أهلا بك في "),
                        TextSpan(text: "أنرني",style: TextStyle(color: Color(0xffd2da32))),
                      ]),)
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Center(child: RichText(textDirection: TextDirection.rtl,textAlign: TextAlign.center,text: TextSpan(children: [
                  TextSpan(text: "أنرني تطبيق خدمي يهدف الى تعريف منسوبي جامعة طيبة بمكتبة الجامعة وأهم الخدمات المتاحة."+"\n\n"+"تم عمل هذا التطبيق كمشروع تخرج لطالبات قسم المعلومات ومصادر التعلم"+"\n"),
                  TextSpan(text: "تحت إشراف د.ريهام غنيم",style: TextStyle(color: Color(0xffd2da32)))
                ],style: TextStyle(color: Colors.white),))),
              ),
              Expanded(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 32),
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Wrap(
                        children:[ InkWell(
                          onTap: (){
                            _navigateToHome(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.white,width: 0.5)
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 16),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 16),
                                      child: Text("أبدا من هنا",style: TextStyle(color: Colors.white,fontSize: 18)),
                                    ),
                                    Image.asset("assets/intro_next.png",width: 30,height: 30,)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )],
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
