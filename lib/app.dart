import 'package:anerny/data/data.dart';
import 'package:anerny/ui/splash.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'أنرني',
      theme: ThemeData(
          primaryColor: Color(0xff6A9AA8), canvasColor: Color(0xff6F9AA3)),
      home: SplashPage(),
    );
  }
}

