import 'dart:math';

import 'package:anerny/ui/info_bank.dart';
import 'package:anerny/ui/intro.dart';
import 'package:anerny/ui/last_news.dart';
import 'package:anerny/ui/parts/base_scafold.dart';
import 'package:anerny/ui/video_player_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/material.dart';

class AutoIndexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);
    return BaseScaffold(
      body: CustomPaint(
        painter: ShapesPainter(),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: eqH(1.5),
                left: eqW(1.5),
                width: eqW(14),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        shape: BoxShape.circle,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Image.asset(
                              "assets/auto_index_logo.png",
                              width: 50,
                            ),
                            Text(
                              "الفهرس الآلي",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22,
                                  color: Colors.black),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                  top: eqH(10),
                  left: eqW(2),
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    "التعريف بالفهرس الآلي",
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  )),
              Positioned(
                  top: eqH(12),
                  left: eqW(6),
                  child: Transform.rotate(
                    angle: pi / 4,
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (_)=>VideoPlayerPage(videoAssetsPath: "assets/vid.mp4",)));
                      },
                      child: Container(
                        width: 120,
                        height: 120,
                        margin: EdgeInsets.all(15.0),
                        padding: EdgeInsets.all(3.0),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black, width: 2)),
                        child: Transform.rotate(
                            angle: -pi / 4,
                            child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Icon(Icons.play_circle_filled,size: 40,),
                                Text(
                                  "Video",
                                  style: TextStyle(
                                      fontSize: 20, fontWeight: FontWeight.bold),
                                ),
                              ],
                            ))),
                      ),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

class HomeButton extends StatelessWidget {
  final double x;
  final double y;
  final String imagePath;
  final String text;
  final Function onTap;

  HomeButton({this.x, this.y, this.imagePath, this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);
    return Positioned(
      top: y,
      left: x,
      width: eqW(2.5),
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                imagePath,
                width: eqW(1.8),
                height: eqH(1.8),
              ),
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var eqH = (y) => y * (size.height / 25);
    var eqW = (x) => x * (size.width / 14);

    final paint = Paint();
    final path = Path();

    path.reset();
    paint.color = Color(0xff9CC7CE);
    canvas.drawRect(
        Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height)), paint);

    path.reset();
    paint.color = Color(0xff6F9AA3);
    path.moveTo(eqW(0), eqH(0));
    path.lineTo(eqW(14), eqH(0));
    path.lineTo(eqW(14), eqH(10.5));
    path.lineTo(eqW(0), eqH(4.5));
    canvas.drawPath(path, paint);

    path.reset();
    paint.color = Color(0xff4C5469);
    canvas.drawRect(
        Rect.fromPoints(
            Offset(0, size.height - eqH(6)), Offset(size.width, size.height)),
        paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
